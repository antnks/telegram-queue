/*
	Telegram queue daemon
*/

use std::net::{TcpListener, TcpStream};
use std::io::{BufRead, BufReader, Read, Write};
use std::{thread, time};

use serde_json::json;
use serde_json::Value;
use std::fs;

use lettre::{Message, SmtpTransport, Transport};

use log::{error, LevelFilter};
use systemd_journal_logger::JournalLog;

static VERSION: &'static str = "1.2.1";


fn tcp_client(mut stream: TcpStream) -> String
{
	//println! ("Got a connection");

	let mut data = String::new();
	let mut reader = BufReader::new(stream.try_clone().unwrap());
	match reader.read_line(&mut data)
	{
		Ok(_) => (),
		Err(err) =>
		{
			println!("TCP cleint read error: {}", err);
			error!("TCP cleint read error: {}", err);
		}
	}
	stream.write (b"Done").ok();

	data.trim().to_string()
}

fn web_post(apiurl: &str, payload: &str) -> String
{
	let empty_string = String::from("");
	// need to create a copy of param because we will pass it into function and rust does not allow that
	let payload_copy = String::from(payload);

	let client = reqwest::blocking::Client::new();
	let res = client.post(apiurl).header("Content-Type", "application/json").body(payload_copy).send();

	// check web request result
	let mut res = match res
	{
		Ok(response) => response,
		Err(err) =>
		{
			println!("Error response: {:?}", err);
			error!("Error response: {:?}", err);
			return empty_string;
		}
	};

	// check if response has body
	let mut res_body = String::new();
	match res.read_to_string(&mut res_body)
	{
		 Ok(_) => (),
		 Err(err) =>
		 {
			println!("Error body: {:?}", err);
			error!("Error body: {:?}", err);
		 }
	}

	return res_body;
}

fn fallback_email(subj: &str, body: &str, email_from: &str, email_to: &str)
{
	println!("{} {} {} {}", subj, body, email_from, email_to);

	let mut body_t = body;
	let mut subj_t = subj;

	// under some unknown circumstances address values may become corrupted
	// add explicit check to avoid panic (issue #5)
	let mut email_to_t = email_to.parse();
	let mut email_from_t = email_from.parse();
	match (email_to_t.clone(), email_from_t.clone())
	{
		(Ok(_), Ok(_)) => (),
		_ =>
		{
			println!("Could not parse address(s): {} {}", email_to, email_from);
			error!("Could not parse address(s): {} {}", email_to, email_from);
			email_to_t = "root <root@localhost>".parse();
			email_from_t = "root <root@localhost>".parse();
			subj_t = "tgq error";
			body_t = "Failed to send telegram message, as well as failure falling back";
		}
	}

	let email = Message::builder()
		.from(email_from_t.unwrap())
		.to(email_to_t.unwrap())
		.subject(subj_t)
		.body(String::from(body_t))
		.unwrap();

	// TODO: change default timeout
	let mailer = SmtpTransport::unencrypted_localhost();

	match mailer.send(&email)
	{
		Ok(_) => println!("Email sent successfully!"),
		Err(e) =>
		{
			println!("Could not send email: {:?}", e);
			error!("Could not send email: {:?}", e);
		}
	}
}

fn main() -> std::io::Result<()>
{
	JournalLog::new().unwrap().install().unwrap();
	log::set_max_level(LevelFilter::Error);

	let config_file = fs::File::open("/etc/telegram.json").expect("Create config first");
	let json: serde_json::Value = serde_json::from_reader(config_file).expect("Config must be JSON");

	let telegram_bot_api = json.get("TELEGRAM_BOT_API").expect("Config must contai TELEGRAM_BOT_API").as_str().unwrap();
	let telegram_chat = json.get("TELEGRAM_CHAT").expect("Config must contain TELEGRAM_CHAT").as_str().unwrap();
	let email_from = json.get("EMAIL_FROM").expect("Config must contain EMAIL_FROM").as_str().unwrap();
	let email_to = json.get("EMAIL_TO").expect("Config must contain EMAIL_TO").as_str().unwrap();

	println! ("Telegram queue daemon started {}", VERSION);

	let apiurl = format!("https://api.telegram.org/bot{}/sendmessage", telegram_bot_api);
	println! ("API URL: {}", apiurl);

	// TODO: handle port busy
	let listener = TcpListener::bind("127.0.0.1:11111")?;

	for stream in listener.incoming()
	{
		let b = tcp_client (stream?);
		//println!("Received: {}", b);
		
		let mut delim = b.splitn(2, " ");
		let subj = delim.next().unwrap();
		let mut body = delim.next();
		if let None = body
		{
			//println!("Got empty body");
			error!("Got empty body");
			body = Some("_");
		}
		let body = body.unwrap();
		//println!("Received: {} - {}", subj, body);

		let payload = format!("{} {}", subj, body);
		let payload_json = json!({"chat_id": telegram_chat, "text": payload});

		// try to send web API request, retry 3 times
		let mut res_body = String::from("");
		for _i in 0..3
		{
			res_body = web_post(apiurl.clone().as_str(), payload_json.to_string().as_str());
			if res_body.len() > 0
			{
				break
			}
			println!("Could not reach API. Doing sleep and retry...");
			error!("Could not reach API. Doing sleep and retry...");
			thread::sleep(time::Duration::from_millis(1000));
		}

		// still no luck?
		if res_body.len() < 1
		{
			println!("We retried, but still cannot reach API");
			error!("We retried, but still cannot reach API");
			fallback_email(subj, body, email_from, email_to);
			continue
		}

		//println!("Status: {}", res.status());
		//println!("Headers:\n{:#?}", res.headers());
		println!("Body:\n{}", res_body);

		// try to parse Body as json
		let res_json: Value = match serde_json::from_str(res_body.as_str())
		{
			Ok(res_json) => res_json,
			Err(_error) =>
			{
				println!("Error json: {:?}", _error);
				error!("Error json: {:?}", _error);
				fallback_email(subj, body, email_from, email_to);
				continue
			}
		};

		// look for "ok" status in json
		match &res_json["ok"]
		{
			json!(true) => continue,
			_ =>
			{
				println!("Json not ok");
				error!("Json not ok");
			}
		}

		// telegram send failed, fallback to email
		fallback_email(subj, body, email_from, email_to);
	}

	Ok(())
}

