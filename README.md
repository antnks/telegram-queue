# tgq - Telegram queue

A daemon that sends telegram bot messages or queues them over email


# Config

Example of a config file `/etc/telegram.json`:

```
{
	"TELEGRAM_BOT_API": "xxxxx",
	"TELEGRAM_CHAT": "11111",
	"EMAIL_FROM": "root <root@localhost>",
	"EMAIL_TO": "root <root@localhost"
}
```


# Dependencies

You may need install: `apt-get install pkg-config libssl-dev`

Compile proces is memory hungry, so you may need to retry it multiple times:

```
[ 4156.164437] Out of memory: Kill process 5111 (rustc) score 297 or sacrifice child
[ 4156.167030] Killed process 5111 (rustc) total-vm:431600kB, anon-rss:230544kB, file-rss:13252kB
```


# Compile

```
cargo build
```

To reduce risk of system instability you can lower thread amount:

```
cargo build -j 1
```

